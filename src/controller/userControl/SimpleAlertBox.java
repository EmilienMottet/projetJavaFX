/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.userControl;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.Vestiare;

/**
 *
 * @author emimo
 */
public class SimpleAlertBox implements AfficheBattle{
    
    
    
    
    @Override
    public void deroulement(Vestiare battleground){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Resultat de la partie");
            alert.setHeaderText("Le vainqueur est ... ");
//            alert.setContentText("Ici s'affichera le resultat de la partie");
            // faire un binding sur une stringproperty qui sort de battle
            
            alert.contentTextProperty().bind(battleground.getBattle().messageProperty());
            
            Stage s = (Stage) alert.getDialogPane().getScene().getWindow();
            s.getIcons().add(new Image(this.getClass().getResource("/view/img/cup.png").toString()));
            
            alert.showAndWait();
    }
    
}
