/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.userControl;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import model.Vestiare;
import org.controlsfx.dialog.ProgressDialog;

/**
 *
 * @author emimo
 */
public class DialogBoxBar implements AfficheBattle{

    @Override
    public void deroulement(Vestiare battleground) {
        Service<Void> service = creerService(battleground);

        ProgressDialog dialog = new ProgressDialog(service);
        dialog.setResizable(true);
        dialog.setTitle("Resultat de la parite");        
        dialog.setHeaderText("Le vainqueur est ... ");
        
        StringProperty mess = new SimpleStringProperty();
        
        mess.bindBidirectional(battleground.getBattle().messageProperty());
        mess.addListener((obs,old,newV) -> {
            if(newV != null){
                Platform.runLater(() ->  dialog.setContentText(newV+"\n"+dialog.getContentText()));
            }
        });
        service.start();
        dialog.showAndWait();
        
    }


    /**
     * creer un thread qui contient une progresse bar
     * @param battleground
     * @return 
     */
    private Service<Void> creerService(Vestiare battleground){
        return new Service<Void>() {
        @Override
        protected Task<Void> createTask() {
            return new Task<Void>() {
                    @Override
                    protected Void call()
                            throws InterruptedException {
                        updateMessage("Match en cours . . .");
                        while(battleground.getBattle().getWinner() == null ){
                            Thread.sleep(100);
                            updateProgress(battleground.getJoueur1().getTeam().getPvTeam(), battleground.getJoueur1().getTeam().getPvTeam() + battleground.getJoueur2().getTeam().getPvTeam());
                        }
                        Thread.sleep(3000);
                        return null;
                    }
                    
                };
            }
        };
    }
    
}
