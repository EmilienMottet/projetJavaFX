/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.userControl;

import controller.FXMLDocumentController;
import controller.popUp.PopUpController;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import launch.LaunchPopUp;
import model.Vestiare;

/**
 * n'est pas opérationnel et pas utilisé
 * @author emimo
 */
public class PoPUpBattle extends PopUpController implements Initializable{

    @FXML
    private VBox corpsMessage;
    
    @FXML
    private ProgressBar progressBar;
    
    private FXMLDocumentController fXMLDocumentController;
    private final StringProperty message = new SimpleStringProperty();
    
    public PoPUpBattle(FXMLDocumentController fXMLDocumentController) {
        this.fXMLDocumentController = fXMLDocumentController;
    }

    public PoPUpBattle() {
    
    }
    
    

    
    
    
    public void deroulement(Vestiare battleground) {
        
        System.out.println("progress bar :"+progressBar+" vbox "+corpsMessage);
        
    /*
        ListView<String> listMessages = new ListView<>();
        ObservableList<String> observableList = FXCollections.observableArrayList();
        listMessages.itemsProperty().setValue(observableList);
        corpsMessage.getChildren().add(listMessages);
        
        message.bindBidirectional(battleground.getBattle().messageProperty());
        
        message.addListener((obs,old,newV) -> {if(newV != null ) { observableList.add(0, newV); }});
      */  
    }
    
    

    public String getMessage() {
        return message.get();
    }

    public void setMessage(String value) {
        message.set(value);
    }

    public StringProperty messageProperty() {
        return message;
    }
    
    public PoPUpBattle lancerFenetre(){
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLBattle.fxml"));
        try {

            new LaunchPopUp().start(new Stage(),fxmlLoader,"Ajouter un nouveau joueur",fXMLDocumentController);
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
}
