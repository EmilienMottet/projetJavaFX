/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.userControl;

import controller.FXMLDocumentController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.Vestiare;

/**
 * pas opérationnel et pas utilisé
 * @author emimo
 */
public class ControllerBattleChoice {

    private Vestiare battleground;
    
    //placer un pattern strategie ici avec les different type d'affichage
    public ControllerBattleChoice(Vestiare battleground, AfficheBattle afficheBattle) {
        this.battleground = battleground;
        try {
            battleground.startGame();
            afficheBattle.deroulement(battleground);
            battleground.reset();
        } catch (Exception ex) {
            enCasDException(ex);
        }
    }
    
    
    
    private void enCasDException(Exception ex){
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("La partie ne peut etre lancée");
            alert.setHeaderText(ex.getMessage());
            alert.setContentText("Veuiller ajuster vos parametre pour pouvoir lancer une partie");
            
            Stage s = (Stage) alert.getDialogPane().getScene().getWindow();
            s.getIcons().add(new Image(this.getClass().getResource("/view/img/cup.png").toString()));
            
            
            
            alert.showAndWait();
    }

}
