/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.userControl;

import controller.FXMLDocumentController;
import model.Vestiare;

/**
 *
 * @author emimo
 */
public class AdapterPopUpBattle implements AfficheBattle{
    
    PoPUpBattle poPUpBattle;
    
    public AdapterPopUpBattle(FXMLDocumentController fXMLDocumentController) {
        poPUpBattle = new PoPUpBattle(fXMLDocumentController);
    }

    
    
    @Override
    public void deroulement(Vestiare vestiare) {
        poPUpBattle = poPUpBattle.lancerFenetre();
        poPUpBattle.deroulement(vestiare);
    }
    
}
