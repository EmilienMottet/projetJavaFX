package controller.userControl;

import java.io.IOException;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import model.unite.stats.StatUnite;

public class CustomControl extends HBox {
    @FXML private Label textField;
    @FXML private Spinner<Integer> spin;
    
    public CustomControl() {
//        System.out.println(getClass().getResource("/view/userControl/custom_control.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/userControl/statsUnite.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        
    }
    
    public CustomControl(String text){
        this();
        setText(text);
    }
    
    public CustomControl(StatUnite statUnite){
        this();
        spin.getValueFactory().valueProperty().bindBidirectional(statUnite.valeurProperty());
        textField.textProperty().bindBidirectional(statUnite.nomProperty());
    }

    public String getText() {
        return textProperty().get();
    }

    public void setText(String value) {
        textProperty().set(value);
    }

    public StringProperty textProperty() {
        return textField.textProperty();
    }
}