/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.popUp;

import controller.FXMLDocumentController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 *
 * @author emimo
 */
public abstract class PopUpController implements Initializable{
    protected Stage stage;
    
    protected FXMLDocumentController mainController;

    @FXML
    protected Button valider;
    
    public FXMLDocumentController getMainController() {
        return mainController;
    }

    public void setMainController(FXMLDocumentController mainController) {
        this.mainController = mainController;
    }
    
    
    
    public void setStage(Stage s){
        stage = s;
    }
    
    public Stage getStage(){
        return stage;
    }
    
    @FXML
    private void valider(){
        
        // soit on fait un switch en testant le nom de la fenetre soit on peut essayer de poser un pattern ? a discutÃ©
        
        stage.close();
    }
    
    @FXML
    private void annuler(){
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources){
        
    }

    
}
