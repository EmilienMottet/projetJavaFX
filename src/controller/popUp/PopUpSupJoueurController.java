/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.popUp;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 * @author emimo
 */
public class PopUpSupJoueurController extends PopUpController{

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        valider.setText("Supprimer");
        valider.setOnAction(eve -> {
            supprimer();
        });
    }

    private void supprimer() {
        stage.close();
    }
    
}
