    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.popUp.PopUpModifCombattantController;
import controller.popUp.PopUpModifJoueurController;
import controller.popUp.PopUpSupCombattantController;
import controller.popUp.PopUpSupJoueurController;
import controller.userControl.ControllerBattleChoice;
import controller.userControl.CustomControl;
import controller.userControl.DialogBoxBar;
import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import launch.LaunchPopUp;
import model.Vestiare;
import model.joueur.Joueur;
import model.joueur.JoueurHumain;
import model.joueur.JoueurIA;
import model.joueur.Team;
import model.unite.Unite;
import model.unite.stats.StatUnite;

/**
 *
 * @author emimo
 */
public class FXMLDocumentController implements Initializable {
    
    private Stage stage;
    
    private Stage stagePrincipal;

    @FXML
    private TableColumn<Joueur, String> nomColumn;
    
    @FXML
    private TableColumn<Joueur, String> victoireColumn;
    
    @FXML
    private TableColumn<Joueur, String> defaiteColumn;
    
    @FXML
    private TableColumn<Joueur, String> typeColumn;
    
    @FXML
    private TableColumn<Joueur, String> nbVictimeColumn;
    
    @FXML
    private TableColumn<Joueur, String> nbPerteColumn;
    
    private Unite uSelect;
    
    @FXML
    private TextField comboEleve;
    
    @FXML
    private TextField comboProf; 
    
    @FXML
    private VBox descripPerso;
    
    @FXML
    private CheckBox checkProf;

    @FXML
    private CheckBox checkEleve;
    
    @FXML
    private CheckBox checkEquip1;
    
    @FXML
    private CheckBox checkEquip2;
    
    @FXML
    private Spinner<Integer> nbTeam;
    
    @FXML
    private TableView<Joueur> tableviewStats;
    
    @FXML
    private Label nomUniteSelect;
    
    @FXML
    private ImageView imageUnite;
    
    @FXML
    private ListView<Unite> liAllCombattant;
    
    @FXML
    private ListView<Unite> liEleve;

    @FXML
    private ListView<Unite> liProf;
    
    @FXML
    private Label ptsCompetence;
    
    
    
    // voir les usercontroller
    @FXML
    private VBox vBoxStats;
   
    

    @FXML
    private Label labequipe1;

    

    private Vestiare battleground = new Vestiare();
    
   
    
    @FXML
    private Label labequipe2;    
    
    
    /**
     * ajoute ou supprime des unites dans dans l'équipe 1 (eleve)
     * @param event 
     */
    private void testCheckBox1(ActionEvent event){
        testcheckBoxTeam(checkEquip1, battleground.getJoueur1());
    }
    
    /**
     * ajoute ou supprime des unites dans dans l'équipe 2 (prof)
     * @param event 
     */
    private void testCheckBox2(ActionEvent event){
        testcheckBoxTeam(checkEquip2, battleground.getJoueur2());
    }

    /**
     * factorisation des ajouts/suppressions dans la team
     * @param box
     * @param j 
     */
    private void testcheckBoxTeam(CheckBox box, Joueur j){
        if(box.isSelected())
            j.getTeam().addUnite(uSelect);
        else
            j.getTeam().removeUnite(uSelect);
    }

    /**
     * active ou désactive l'IA sur un joueur eleve
     * @param event 
     */
    private void testCheckBoxEleve(ActionEvent event){
        testCheckBoxIA(checkEleve, battleground.getJoueur1());
    }
    
    /**
     * active ou désactive l'IA sur un joueur prof
     * @param event 
     */
    private void testCheckBoxProf(ActionEvent event){
        testCheckBoxIA(checkProf, battleground.getJoueur2());
    }
    /**
     * factorisation des checkbox sur IA
     * @param box
     * @param j 
     */
    private void testCheckBoxIA(CheckBox box, Joueur j){
        if(! box.isSelected())
            j.setIntelligence(new JoueurIA());
        else
            j.setIntelligence(new JoueurHumain());
    }
    
    /**
     * ouvre une fenetre pour ajouter des joueurs (pas implementer)
     * @param event 
     */
    @FXML
    private void addJoueur(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLaddJoueur.fxml"));
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Ajouter un nouveau joueur",this);
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * ouvre une fenetre pour modifier des joueurs (pas implementer)
     * @param event 
     */
    @FXML
    private void modifJoueur(ActionEvent event){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLmodifJoueur.fxml"));
        
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Modifier un joueur",this,new PopUpModifJoueurController());
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * ouvre une fenetre pour supprimer des joueurs (pas implementer)
     * @param event 
     */
    @FXML
    private void supJoueur(ActionEvent event){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLmodifJoueur.fxml"));
        
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Supprimer un joueur",this,new PopUpSupJoueurController());
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * ouvre une fenetre pour ajouter des unites (pas implementer)
     * @param event 
     */
    @FXML
    private void addCombattant(ActionEvent event){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLaddCombattant.fxml"));
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Ajouter un combattant",this);
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * ouvre une fenetre pour modifier des unites (pas implementer)
     * @param event 
     */
    @FXML
    private void modifCombattant(ActionEvent event){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLmodifCombattant.fxml"));
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Modifier un combattant",this,new PopUpModifCombattantController());
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * ouvre une fenetre pour supprimer des unites (pas implementer)
     * @param event 
     */
    @FXML
    private void supCombattant(ActionEvent event){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/FXMLmodifCombattant.fxml"));
        try {
            new LaunchPopUp().start(new Stage(),fxmlLoader,"Supprimer un joueur",this,new PopUpSupCombattantController());
        } catch (Exception ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * ouvre une fenetre pour sauvegarder des compositions(pas implementer)
     * @param event 
     */
    @FXML
    private void vueSave(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Fichier de sauvegarde", "*.xml"),
                new FileChooser.ExtensionFilter("Tous les fichiers", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(new Stage());
        if (selectedFile != null) {
           //action
        }
    }
    
    /**
     * correspond au clic sur le bouton lancer partie et lance la partie
     * @param event 
     */
    @FXML
    private void startGame(ActionEvent event){
        ControllerBattleChoice controllerBattle = new ControllerBattleChoice(battleground,new DialogBoxBar());
    }
    
    
    /**
     * iniatilise le controller
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bindSurBattleground();
        creationEcouteurListAll();
        modifCell();
        eventCheckBox();
    }

    /**
     * fais des bindings
     */
    private void bindSurBattleground(){
        bindingBase();
        bindDisable();
    }
    
    /**
     * défais des bindings sur le joueur eleve
     */
    private void unbindJoueurEleve(Joueur oldV){
        labequipe1.textProperty().unbindBidirectional(oldV.nomProperty());
        battleground.joueur1Property().unbindBidirectional(oldV.nomProperty());
    }
    
    /**
     * fais des bindings sur le joueur eleve
     */
    private void bindJoueurEleve(Joueur newV){
        labequipe1.textProperty().bindBidirectional(newV.nomProperty());
        
        battleground.joueur1Property().bindBidirectional(newV.nomProperty());
    }
    
    /**
     * défais des bindings sur le joueur prof
     */
    private void unbindJoueurProf(Joueur oldV){
        labequipe2.textProperty().unbindBidirectional(oldV.nomProperty());
        battleground.joueur2Property().unbindBidirectional(oldV.nomProperty());
    }
    
    /**
     * fais des bindings sur le joueur prof
     */
    private void bindJoueurProf(Joueur newV){
        labequipe2.textProperty().bindBidirectional(newV.nomProperty());
        battleground.joueur2Property().bindBidirectional(newV.nomProperty());
    }
    
    
    /**
     * place les event onaction sur les checkbox
     */
    private void eventCheckBox(){
        checkEquip1.setOnAction(e -> testCheckBox1(e));
        checkEquip2.setOnAction(e -> testCheckBox2(e));
        checkEleve.setOnAction(e -> testCheckBoxEleve(e));
        checkProf.setOnAction(e -> testCheckBoxProf(e));        
    }
    
    /**
     * creer l'ecouteur sur la list de tout les combattants
     */
    private void creationEcouteurListAll(){
        liAllCombattant.getSelectionModel().selectedItemProperty().addListener((obs,old,newV) -> {
            if (old != null) {
                unbindSurElementsUnite(old);
            }
            if (newV != null) {
                bindSurElementsUnite(newV);
            }
        });
    }
    
    /**
     * defais des bindings
     */
    private void bindDisable(){
        descripPerso.disableProperty().bind(liAllCombattant.getSelectionModel().selectedIndexProperty().isEqualTo(-1));
        checkEquip1.disableProperty().bind(battleground.getJoueur1().getTeam().isFullProperty().and(checkEquip1.selectedProperty().not()));
        checkEquip2.disableProperty().bind(battleground.getJoueur2().getTeam().isFullProperty().and(checkEquip2.selectedProperty().not()));
        
    }
    
    /**
     * fais des bindings
     */
    private void bindingItemProperty(){
        liEleve.itemsProperty().bind(battleground.getJoueur1().getTeam().listUnitProperty());
        liProf.itemsProperty().bind(battleground.getJoueur2().getTeam().listUnitProperty());
        liAllCombattant.itemsProperty().bind(battleground.getCoAlUn().listAllUnitsProperty());
        comboEleve.textProperty().bindBidirectional(battleground.getJoueur1().nomProperty());
        comboProf.textProperty().bindBidirectional(battleground.getJoueur2().nomProperty());
        tableviewStats.itemsProperty().bind(battleground.getCoAlJou().listAllJoueursProperty());
    }
    
    /**
     * fais des bindings
     */
    private void bindingBase(){
        bindingItemProperty();
        nomColumn.setCellValueFactory(cellData -> cellData.getValue().nomProperty());       
        checkEquip1.textProperty().bindBidirectional(battleground.getJoueur1().nomProperty());
        checkEquip2.textProperty().bindBidirectional(battleground.getJoueur2().nomProperty());
        
        bindStatic();
        
    }
    /**
     * fais des bindings sur les stats avec un customcontrol
     */
    private void bindingStatistique(Unite u){

        vBoxStats.getChildren().clear();
        if(u != null)
            for (StatUnite  stat : u.getListStats()) {
                vBoxStats.getChildren().add(new CustomControl(stat));
            }
    }   
    /**
     * fais des bindings ( sur le nb de joueur dans les teams )
     */
    private void bindStatic(){
        nbTeam.getValueFactory().valueProperty().bindBidirectional(Team.tailleProperty());
        nbTeam.getValueFactory().valueProperty().addListener((o,s,q) -> {        // changelistener + interssant que le invalidation car le binding sur la donnÃƒÂ© est fait avant l'event
            battleground.getJoueur1().getTeam().normerTeam();
            battleground.getJoueur2().getTeam().normerTeam();
        }
        );
    }
    
    /**
     * fais les cellfactory
     */
    private void modifCell(){
        liEleve.setCellFactory(param -> modifListEcranStart()  );
        liProf.setCellFactory(param -> modifListEcranStart()  );
        
        cellAllCom();
    }
    
    /**
     * fais les cellfactory
     */
    private void cellAllCom(){
        liAllCombattant.setCellFactory(param -> new ListCell<Unite>(){
            
            @Override
            protected void updateItem(Unite item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    setText(item.getNom());
                } else {
                    setText("");
                }
            }
            
        });
    }

    /**
     * fais les cellfactory
     * @return 
     */
    private ListCell<Unite> modifListEcranStart(){
        return new ListCell<Unite>(){
            
            @Override
            protected void updateItem(Unite item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    textProperty().bind(item.nomProperty());
                } else {
                    textProperty().unbind();
                    setText("");
                }
                setBackground(Background.EMPTY);
            }
        };
    }

    /**
     * fais des unbind sur l'unite
     * @param old 
     */
    private void unbindSurElementsUnite(Unite old) {
        imageUnite.imageProperty().unbindBidirectional(old.imageProperty());
        nomUniteSelect.textProperty().unbindBidirectional(old.nomProperty());
        nomUniteSelect.setText("");
        checkEquip1.setSelected(false);
        checkEquip2.setSelected(false);
        Bindings.unbindBidirectional(ptsCompetence.textProperty(), old.pointDeCompetenceADistribProperty());
        uSelect = null;
        bindingStatistique(null);
    }

    /**
     * fais des bind sur l'unite
     * @param old 
     */
    private void bindSurElementsUnite(Unite newV) {
        imageUnite.imageProperty().bindBidirectional(newV.imageProperty());
        nomUniteSelect.textProperty().bindBidirectional(newV.nomProperty());
        checkEquip1.setSelected(battleground.getJoueur1().getTeam().listUnitProperty().contains(newV));
        checkEquip2.setSelected(battleground.getJoueur2().getTeam().listUnitProperty().contains(newV));
        Bindings.bindBidirectional(ptsCompetence.textProperty(), newV.pointDeCompetenceADistribProperty(), new  DecimalFormat());
        uSelect = newV;
        bindingStatistique(newV);
    }
    
    /**
     * set le stage principal
     * @param s 
     */
    public void setStagePrincipal(Stage s){
        stagePrincipal = s;
    }
    /**
     * return le stage principal
     * @return 
     */
    public Stage getStagePrincipal(){
        return stagePrincipal;
    }
}
