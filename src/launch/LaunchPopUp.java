/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launch;

import controller.FXMLDocumentController;
import controller.popUp.PopUpController;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author emimo
 */
public class LaunchPopUp  {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage,FXMLLoader fxmlLoader, String nom,FXMLDocumentController  mainController) throws Exception{
        Parent root1 = (Parent) fxmlLoader.load();
        
        PopUpController pC = fxmlLoader.getController();
        pC.setStage(stage);
        pC.setMainController(mainController);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UTILITY);
        stage.initOwner(mainController.getStagePrincipal());
        stage.setTitle(nom);
        stage.setScene(new Scene(root1));  
        stage.show();
    }
    
    public void start(Stage stage,FXMLLoader fxmlLoader, String nom, FXMLDocumentController mainController, PopUpController controllerFenetre) throws Exception{
        fxmlLoader.setController(controllerFenetre);
        start(stage, fxmlLoader, nom, mainController);
        
    }
    
}
