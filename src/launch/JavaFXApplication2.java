/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package launch;

import controller.FXMLDocumentController;
import impl.org.controlsfx.i18n.Localization;
import java.util.Locale;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 *
 * @author emimo
 */
public class JavaFXApplication2 extends Application {
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }    
    @Override
    public void start(Stage stage) throws Exception {
        Localization.setLocale(Locale.FRANCE);
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLDocument.fxml"));
        
        Parent root = loader.load();
        FXMLDocumentController controller = loader.getController();
        controller.setStagePrincipal(stage);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/view/style.css").toExternalForm());
        
        stage.setTitle("Student vs Professor");
        stage.setMinHeight(450.);
        stage.setMinWidth(600.0);
        stage.getIcons().add(new Image(this.getClass().getResource("/view/img/icon.png").toString()));
        stage.setScene(scene);
        stage.show();
        
    }
}
