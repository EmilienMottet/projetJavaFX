
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.Collection.CollectionJoueur;
import model.Collection.CollectionUnites;
import model.Combat.Battle;
import model.joueur.FabriqueProfession;
import model.joueur.Joueur;
import model.joueur.JoueurIA;
import model.unite.JoueurEleve;
import model.unite.JoueurProf;
import model.unite.action.Action;
import model.unite.action.Attaque;
import model.unite.action.DistanceUnite;

/**
 *
 * @author emimo
 */
public class Vestiare implements Observer{
    private CollectionUnites coAlUn = new CollectionUnites();
    
    private CollectionJoueur coAlJou = new CollectionJoueur();
    
    private final ObjectProperty<Joueur> joueur1 = new SimpleObjectProperty<>();
    private final ObjectProperty<Joueur> joueur2 = new SimpleObjectProperty<>();
    private Battle battle;
    public Vestiare() {
        FabriqueProfession prof = new JoueurProf();
        FabriqueProfession eleve = new JoueurEleve();
        setJoueur1(coAlJou.recup(new Joueur("Joueur 1 ( eleve )",prof)));
        setJoueur2(coAlJou.recup(new Joueur("Joueur 2 ( prof )",eleve)));
        
        
        if(getJoueur1() == null)
            setJoueur1(new Joueur("Joueur1",prof));
        if(getJoueur2() == null)
            setJoueur2(new Joueur("Joueur2",eleve));
        
        getJoueur1().getTeam().setColU(coAlUn);
        getJoueur2().getTeam().setColU(coAlUn);
        getJoueur1().setIntelligence(new JoueurIA());
        getJoueur2().setIntelligence(new JoueurIA());
    }

    public Joueur getJoueur1() {
        return joueur1.get();
    }

    public void setJoueur1(Joueur value) {
        joueur1.set(value);
    }

    public ObjectProperty joueur1Property() {
        return joueur1;
    }

    public CollectionUnites getCoAlUn() {
        return coAlUn;
    }

    public Joueur getJoueur2() {
        return joueur2.get();
    }

    public void setJoueur2(Joueur value) {
        joueur2.set(value);
    }

    public ObjectProperty joueur2Property() {
        return joueur2;
    }
    
    

    

    public CollectionJoueur getCoAlJou() {
        return coAlJou;
    }
    

    public Battle getBattle() {
        return battle;
    }
    
    public void startGame() throws Exception{
        if( !getJoueur1().getTeam().isIsFull() || !getJoueur2().getTeam().isIsFull()){
            throw new Exception("pb car les equipes sont pas full");
        }
       
        battle = new Battle(getJoueur1().getTeam(), getJoueur2().getTeam(), DistanceUnite.max, 
                (u1 , u2) -> {
                    Action realiser = new Action(null);
                    System.out.println("u1 : "+u1.getNom()+" esquive : "+u1.getAction());
                    System.out.println("u2 : "+u2.getNom()+" esquive : "+u2.getAction());
                    List<Attaque> li = new ArrayList<Attaque>();
                    if(u2.estEnVie()){
                        for (Attaque attaque : u1.getAction().getAttaques()) {
                            if(attaque.getDistance().estADistance(battle.calculDistance(u1, u2))){
                                li.add(attaque);
                                if( 2 * u1.getAction().getEsquive().getValue() * Math.random() > u2.getAction().getEsquive().getValue() * Math.random() ){
                                    u2.setPointVie(u2.getPointVie() - (attaque.getValue() - u2.getAction().getDefense().getValue()));
                                    realiser.setDefense(u2.getAction().getDefense());
                                }else{
                                    realiser.setEsquive(u2.getAction().getEsquive());
                                }
                                realiser.setAttaques(li);
                                break;
                            }
                        }                        
                    }
                    

                    // on gere les aoe les heal ici
//                    battle.getPositionSurLaColonne(null);
                    
                    return realiser;
                }
        );
        battle.startBattle();
        
    }
    
    public void reset(){
        getJoueur1().getTeam().reset();
        getJoueur2().getTeam().reset();
        battle = null;
        System.gc();
    }
    
    
    // pb si diferent thread il ne peut y avoir de binding

    @Override
    public void update(Observable o, Object arg) {

    }
    
}
