/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Collection;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.joueur.FabriqueProfession;
import model.joueur.Joueur;
import model.unite.JoueurEleve;
import model.unite.JoueurProf;

/**
 *
 * @author emilien
 */
public class CollectionJoueur {
    private ObservableList<Joueur> obsAllJouEleve = FXCollections.observableArrayList();
    private final ListProperty<Joueur> listAllJoueursEleve = new SimpleListProperty<>(obsAllJouEleve) ;
    private ObservableList<Joueur> obsAllJouProf = FXCollections.observableArrayList();
    private final ListProperty<Joueur> listAllJoueursProf = new SimpleListProperty<>(obsAllJouProf) ;
    private ObservableList<Joueur> obsAllJou = FXCollections.observableArrayList();
    private final ListProperty<Joueur> listAllJoueurs = new SimpleListProperty<>(obsAllJou);



    public CollectionJoueur() {
        recupAllJoueur();
    }

    private void recupAllJoueur() {
        FabriqueProfession prof = new JoueurProf();
        FabriqueProfession eleve = new JoueurEleve();
        ajouterJoueur(new Joueur("Joueur 2 ( prof )",prof));
        ajouterJoueur(new Joueur("Joueur 1 ( eleve )",eleve));
        
    }
    
    
    public void ajouterJoueur(Joueur j){
        if(j.getJob() instanceof JoueurEleve){
            obsAllJouEleve.add(j);
        } 
        
        if(j.getJob() instanceof JoueurProf){
            obsAllJouProf.add(j);
        }
        
        obsAllJou.add(j);
        
        // coder de maniere dégueux
    }
        

    public ObservableList getListAllJoueursEleve() {
        return listAllJoueursEleve.get();
    }

    public ListProperty listAllJoueursEleveProperty() {
        return listAllJoueursEleve;
    }


    public ObservableList getListAllJoueursProf() {
        return listAllJoueursProf.get();
    }

    public ListProperty listAllJoueursProfProperty() {
        return listAllJoueursProf;
    }    

    

    public ObservableList getListAllJoueurs() {
        return listAllJoueurs.get();
    }

    public void setListAllJoueurs(ObservableList value) {
        listAllJoueurs.set(value);
    }

    public ListProperty listAllJoueursProperty() {
        return listAllJoueurs;
    }    
    /**
     * 
     * @param u
     * @return 
     */
    public Joueur recup(Joueur u){
        try{
            return obsAllJou.stream().filter( ob -> u.equals(ob)).findFirst().get();
        }catch(Exception e ){
            return null;
        }
    }
    
}
