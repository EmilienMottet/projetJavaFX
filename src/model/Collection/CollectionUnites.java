/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Collection;

import java.util.stream.Collectors;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.unite.FabriqueCombattant;
import model.unite.TypeCombattant;
import model.unite.Unite;

/**
 *
 * @author emilien
 */
public class CollectionUnites {
    
    ObservableList<Unite> obsAllUnites = FXCollections.observableArrayList();
    private final ListProperty<Unite> listAllUnits = new SimpleListProperty<>(obsAllUnites);
    public CollectionUnites(){
        recupToutesUnites();
    }

    public ObservableList getListAllUnits() {
        return listAllUnits.get();
    }

    public void setListAllUnits(ObservableList value) {
        listAllUnits.set(value);
    }

    public ListProperty listAllUnitsProperty() {
        return listAllUnits;
    }
    
    
    
    private void recupToutesUnites(){
        obsAllUnites.add(FabriqueCombattant.getCombattant(TypeCombattant.TANK,"Combattant Tank"));
        obsAllUnites.add(FabriqueCombattant.getCombattant(TypeCombattant.ARCHER,"Combattant Archer"));
        obsAllUnites.add(FabriqueCombattant.getCombattant(TypeCombattant.SOIGNEUR,"Combattant Soigneur"));
        obsAllUnites.add(FabriqueCombattant.getCombattant(TypeCombattant.ASSASSIN,"Combattant Assassin"));
        obsAllUnites.add(FabriqueCombattant.getCombattant(TypeCombattant.MAGE,"combattant Mage"));
        obsAllUnites.setAll(obsAllUnites.stream().sorted((u1,u2) -> u1.getNom().compareTo(u2.getNom())).collect(Collectors.toList()));
        
    }
    
    public Unite recup(Unite u){
        try{
            return obsAllUnites.stream().filter( ob -> u.equals(ob)).findFirst().get();
        }catch(Exception e ){
            return null;
        }
    }
    
    public Unite unitAleatoire(){
        return obsAllUnites.get((int) (Math.random() * obsAllUnites.size() ));
    }
}
