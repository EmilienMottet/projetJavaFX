/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.joueur;

/**
 *
 * @author emimo
 */
public interface Intelligence {
    /**
     * s'occupe de selectionner les unites de la team
     * @param team 
     */
    void gererTeam(Team team);
}
