/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.joueur;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Collection.CollectionUnites;
import model.unite.Unite;

/**
 *
 * @author emimo
 */
public class Team {
    private static final ObjectProperty<Integer> taille = new SimpleObjectProperty<>(1);
    public static Integer getTaille() {
        return taille.get();
    }
    public static void setTaille(Integer value) {
        taille.set(value);
    }
    public static ObjectProperty tailleProperty() {
        return taille;
    }
    private Joueur propietaire;
    private ObservableList<Unite> obsli = FXCollections.observableArrayList();
    private final ListProperty<Unite> listUnit = new SimpleListProperty<>(obsli);
    private CollectionUnites colU;
    private final ReadOnlyBooleanWrapper isFull = new ReadOnlyBooleanWrapper(false);
    public Team(Joueur proprio) {
        propietaire=proprio;
    }

    public Joueur getProprietaire() {
        return propietaire;
    }

    public void setProprietaire(Joueur propietaire) {
        this.propietaire = propietaire;
    }
    
    public void reset(){
        for (Unite unite : obsli) {
            unite.setPointVie(0); // permet de tuer toutes les unitere et de stop le thread
        }
        obsli.clear();
        isFull.set(false);
        getProprietaire().getIntelligence().gererTeam(this);
    }
    

    public ObservableList<Unite> getListUnit() {
        return listUnit.get();
    }

    public void setListUnit(ObservableList value) {
        listUnit.set(value);
    }

    public ListProperty<Unite> listUnitProperty() {
        return listUnit;
    }
    

    public boolean isIsFull() {
        return isFull.get();
    }

    public ReadOnlyBooleanProperty isFullProperty() {
        return isFull.getReadOnlyProperty();
    }

   
    
    public boolean addUnite(Unite unite){
        boolean reussit = false;
        if(testPlace()){            
        Unite u = colU.recup(unite);
        if(u !=null && !obsli.contains(u))
            reussit=obsli.add(u);
        }
        verifPlace();
        return reussit;
    }
    
    public void removeUnite(Unite unite){
        obsli.remove(unite);
        verifPlace();
    }
    
    public void verifPlace(){
        isFull.set(!(obsli.size() <  taille.get()));
    }
    
    public void normerTeam(){
        
        isFull.set(!(obsli.size() <  taille.get()));
        while (obsli.size() >  taille.get()) {
            obsli.remove(obsli.size()-1);
        }
        
        if(propietaire.getIntelligence() != null)
            propietaire.getIntelligence().gererTeam(this);
    }
    
    private boolean testPlace(){
        return obsli.size() <  taille.get();
    }

    public void setColU(CollectionUnites colU) {
        this.colU = colU;
    }

    public CollectionUnites getColU() {
        return colU;
    }
    
    public void typer(){
        for (int i = 0; i < obsli.size(); ++i) {
            obsli.set(i, propietaire.getJob().typer(obsli.get(i))); 
        }
    }

    @Override
    public String toString() {
        return "Team{" + "obsli=" + obsli + '}';
    }
    
    public int getPvTeam(){
        int pvtt = 0;
        for (Unite unite : obsli) {
            pvtt += unite.getPointVie();
        }
        
        return pvtt;
    }
    
}
