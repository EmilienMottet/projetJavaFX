/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.joueur;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author emimo
 */
public class Joueur {
    
    private Intelligence intelligence;


    
    
    private FabriqueProfession job;
    
    private final StringProperty nom = new SimpleStringProperty();
    private Team team;
    public Joueur(String nom, FabriqueProfession profession) {
        setNom(nom);
        team= new Team(this);
        setJob(profession);
    }
    /**
     * set un type d'IA
     * @return 
     */
    public Intelligence getIntelligence() {
        return intelligence;
    }
    public void setIntelligence(Intelligence intelligence) {
        this.intelligence = intelligence;
        this.intelligence.gererTeam(team);
    }

    public String getNom() {
        return nom.get();
    }

    public void setNom(String value) {
        nom.set(value);
    }

    public StringProperty nomProperty() {
        return nom;
    }


    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * return la fabrique en fonction du type de job
     * @return 
     */
    public FabriqueProfession getJob() {
        return job;
    }

    public void setJob(FabriqueProfession job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "Joueur{" + "job=" + job + ", nom=" + nom + ", team=" + team + '}';
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Joueur other = (Joueur) obj;
        if (!Objects.equals(this.getNom(), other.getNom())) {
            return false;
        }
        return true;
    }

    
    
    
    
    
}
