/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.io.File;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import model.unite.action.Action;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.Esquive;
import model.unite.stats.StatUnite;
import model.unite.stats.StatUniteVita;
import model.unite.stats.TypeStat;

/**
 *
 * @author emimo
 */
public abstract class Combattant extends Unite {
    public static final int PVBASE = 100;
    private final StringProperty nom = new SimpleStringProperty();
    private int pointStatTot;
    private final IntegerProperty pointDeCompetenceADistrib = new SimpleIntegerProperty();
    private final BooleanProperty aDistribTouteSesStats = new SimpleBooleanProperty();
    private final ObjectProperty<Image> image = new SimpleObjectProperty<>();
    protected ObservableList<StatUnite> obsStats = FXCollections.observableArrayList();
    private final ListProperty<StatUnite> listStats = new SimpleListProperty<>(obsStats);
    private int pv;
    private Action action;

    protected Combattant(String nom) {
        setPointVie(PVBASE);
        setNom(nom);
        setImage(new Image("file:\\" + new File("").getAbsolutePath() + "\\src\\view\\img\\fighter1.png"));
        setPointStatTot(new Double(Math.random() * 10).intValue() + 8);
        obsStats.add(new StatUnite(this, "defense", 1, TypeStat.DEFENSE));
        obsStats.add(new StatUnite(this, "attaque", 1, TypeStat.ATTAQUE));
        obsStats.add(new StatUnite(this, "vitesse", 1, TypeStat.VITESSE));
        obsStats.add(new StatUnite(this, "esquive", 1, TypeStat.ESQUIVE));
        obsStats.add(new StatUniteVita(this, "vitalite", 1));
        isaDistribTouteSesStats();
    }
    //un protype ca aurait pu etre bien mais on est emmerder a cause des fx properties
    protected Combattant(Combattant unite) {
        setPointVie(unite.getPointVie());
        setImage(unite.getImage());
        setNom(unite.getNom());
        obsStats = FXCollections.observableArrayList(unite.getListStats());
        setListStats(obsStats);
        setPointStatTot(unite.getPointStatTot());
        setaDistribTouteSesStats(unite.isaDistribTouteSesStats());
        
        //obligatoire car si on l'attaque avant qu'il a fini de jouer
        this.action = new Action(getGestionnaireStats()){{ setDefense(defendre()); setEsquive(esquive());}};
    }
    /**
     * les classes filles a décomposer jouer en 4 étapes 
     * 
     * @return une action qui devra etre traiter 
     */
    @Override
    public Action jouer() {
        Action action = new Action(getGestionnaireStats());
        action.setAttaques(attaquer());
        action.setDefense(defendre());
        action.setEsquive(esquive());
        action.setDeplacement(deplacer());

        return action;
    }

    public abstract List<Attaque> attaquer();

    public abstract Defense defendre();

    public abstract Esquive esquive();

    public abstract Deplacement deplacer();

    @Override
    protected Object clone() throws CloneNotSupportedException {
        System.out.println("on est dans le clonage d'un combattant");
        Combattant clone = (Combattant) super.clone();

        return clone;
    }


    @Override
    public String getNom() {
        return nom.get();
    }

    @Override
    public void setNom(String value) {
        nom.set(value);
    }

    @Override
    public StringProperty nomProperty() {
        return nom;
    }


    @Override
    public int getPointStatTot() {
        return pointStatTot;
    }

    private void setPointStatTot(int pointStatTot) {
        this.pointStatTot = pointStatTot;
    }


    @Override
    public int getPointDeCompetenceADistrib() {
        return pointDeCompetenceADistrib.get();
    }

    @Override
    public void setPointDeCompetenceADistrib(int value) {
        pointDeCompetenceADistrib.set(value);
    }

    @Override
    public IntegerProperty pointDeCompetenceADistribProperty() {
        return pointDeCompetenceADistrib;
    }


    public boolean isaDistribTouteSesStats() {
        setPointDeCompetenceADistrib(getPointStatTot() - obsStats.stream().mapToInt(stats -> stats.getValeur()).sum());
        setaDistribTouteSesStats(0 > getPointDeCompetenceADistrib());
        return aDistribTouteSesStats.get();
    }

    private void setaDistribTouteSesStats(boolean value) {
        aDistribTouteSesStats.set(value);
    }

    public BooleanProperty aDistribTouteSesStatsProperty() {
        return aDistribTouteSesStats;
    }


    @Override
    public Image getImage() {
        return image.get();
    }

    @Override
    public void setImage(Image value) {
        image.set(value);
    }

    @Override
    public ObjectProperty imageProperty() {
        return image;
    }


    @Override
    public ObservableList<StatUnite> getListStats() {
        return listStats.get();
    }

    @Override
    public void setListStats(ObservableList value) {
        listStats.set(value);
    }

    @Override
    public ListProperty<StatUnite> listStatsProperty() {
        return listStats;
    }


    @Override
    public boolean estEnVie() {
        return pv > 0;
    }

    @Override
    public void setPointVie(int pv) {
        if (pv < 0) {
            this.pv = 0;
        }
        this.pv = pv;
    }

    @Override
    public int getPointVie() {
        return pv;
    }


    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public void setAction(Action action) {
        this.action = action;
    }
}
