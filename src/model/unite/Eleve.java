/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;


/**
 *
 * @author emimo
 */

public class Eleve extends DecorateurRace{

    public Eleve(Unite unite) {
        super(unite);
        raceProperty().set("Eleve");
    }

    @Override
    public void aptitudeRacial() {
        System.out.println("je suis un Eleve");
    }

    
}
