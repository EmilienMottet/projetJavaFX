/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import model.unite.action.Action;
import model.unite.stats.GestionnaireStats;
import model.unite.stats.StatUnite;

/**
 *
 * @author emimo
 */
public abstract class Unite extends UniteObservable implements Cloneable, Runnable{
    private Thread thread = new Thread(this);


    
    public abstract String getNom();

    public abstract void setNom(String value);

    public abstract StringProperty nomProperty();
    
    
    public abstract int getPointStatTot();        
    

    public abstract int getPointDeCompetenceADistrib();

    public abstract void setPointDeCompetenceADistrib(int value);

    public abstract IntegerProperty pointDeCompetenceADistribProperty();

    public abstract boolean isaDistribTouteSesStats();

    public abstract BooleanProperty aDistribTouteSesStatsProperty();
        
    
    
    
    public abstract Image getImage();

    public abstract void setImage(Image value);

    public abstract ObjectProperty imageProperty();

    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unite other = (Unite) obj;
        if (!Objects.equals(this.getNom(), other.getNom())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Unite{" + "nom=" + getNom() + ", pointStatTot=" + getPointStatTot() + '}';
    }


    

    //inutile
    @Override
    protected Object clone() throws CloneNotSupportedException {
        System.out.println("on est dans le clone d'une unite");
        Unite ret = (Unite) super.clone();
        
        
        return ret;
    }
    
    
    public abstract Action jouer();
    
    public abstract Action getAction();
    
    public abstract void setAction(Action action);
  
    public abstract ObservableList<StatUnite> getListStats();

    public abstract void setListStats(ObservableList value);
    

    public abstract ListProperty<StatUnite> listStatsProperty();
    
    public abstract boolean estEnVie();
    
    public abstract void setPointVie(int pv);
    
    public abstract int getPointVie();
    
    
    /**
     * routine du thread
     */
    @Override
    public void run() {
        while(estEnVie()){
            setAction(jouer());
            notifier();
            try {
                thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Combattant.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("JE MEURT");
        notifier();
    }
    /**
     * lance le thread
     */
    public void lanceThread(){
        thread.start();
    }

    /**
     * retourne un gestionnaire de stat ( ca permet de traiter les stats plus facilements )
     * @return 
     */
    public GestionnaireStats getGestionnaireStats() {
        GestionnaireStats gestionnaireStats = null, gs = null;
        if (!getListStats().isEmpty()) {
            gestionnaireStats = gs = getListStats().get(0);
            for (int i = 1; i < getListStats().size(); i++) {
                gestionnaireStats = gestionnaireStats.setNext(getListStats().get(i));
            }
        }
        return gs;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.thread);
        return hash;
    }
    
    
    
    
}
