/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;


import java.util.ArrayList;
import java.util.List;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.action.Esquive;

/**
 *
 * @author emimo
 */
public class CombattantAssassin extends Combattant{

    CombattantAssassin(String nom) {
        super(nom);
    }
    public CombattantAssassin(CombattantAssassin assassin){
        super(assassin);
    }
    
    @Override
    public List<Attaque> attaquer() {
        return new ArrayList<Attaque>() {{ add(new Attaque(6, "coup de poignard")); }} ;        
    }

    @Override
    public Defense defendre() {
        return new Defense(3, "se protege avec son poignard");
    }
    
    @Override
    public Esquive esquive(){
        return new Esquive(7, "evite le coup");
    }
    
    @Override
    public Deplacement deplacer() {
        return new Deplacement(new DistanceUnite(1), "furtivement");
    }
    
    
}
