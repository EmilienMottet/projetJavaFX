/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;


import java.util.ArrayList;
import java.util.List;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.action.Esquive;
import model.unite.stats.StatUnite;
import model.unite.stats.TypeStat;

/**
 *
 * @author emimo
 */
public class CombattantTank extends Combattant{

    public CombattantTank(String nom) {
        super(nom);
        getListStats().set(0,new StatUnite(this, "Armure",1,TypeStat.DEFENSE));
        getListStats().set(1,new StatUnite(this, "Force",1,TypeStat.ATTAQUE));
        getListStats().set(2,new StatUnite(this, "Vitesse",1,TypeStat.VITESSE));
        getListStats().set(3,new StatUnite(this, "Parade",1,TypeStat.ESQUIVE));
    }
    public CombattantTank(CombattantTank tank){
        super(tank);
    }

    @Override
    public List<Attaque> attaquer() {
        return new ArrayList<Attaque> (){{add(new Attaque(4, "coup de glaive")) ; }};
    }

    @Override
    public Defense defendre() {
        return new Defense(5, "protection avec bouclier");
    }
    
    @Override
    public Esquive esquive(){
        return new Esquive(4, "parade");
    }

    @Override
    public Deplacement deplacer() {
        return new Deplacement(new DistanceUnite(1), "lourd");
    }
    
    
}
