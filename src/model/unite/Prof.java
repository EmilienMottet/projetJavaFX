/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

/**
 *
 * @author emimo
 */
public class Prof extends DecorateurRace{

    public Prof(Unite unite) {
        super(unite);
        raceProperty().set("Prof");
    }

    @Override
    public void aptitudeRacial() {
        System.out.println("je suis un prof");
    }
    
    
    
}
