/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import model.joueur.FabriqueProfession;

/**
 *
 * @author emimo
 */
public class JoueurEleve implements FabriqueProfession{

    @Override
    public Unite typer(Unite unite) {
        return new Eleve(FabriqueCombattant.getCombattant(unite));
    }
    
}
