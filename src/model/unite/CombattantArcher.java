/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.util.ArrayList;
import java.util.List;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.action.Esquive;

/**
 *
 * @author emimo
 */
public class CombattantArcher extends Combattant{

    CombattantArcher(String nom) {
        super(nom);
    }
    public CombattantArcher(CombattantArcher archer){
        super(archer);
    }
    
    
    @Override
    public List<Attaque> attaquer() {       
        return new ArrayList<Attaque> (){{add(new Attaque(9, "tir à l'attaque")) ; }};
    }

    @Override
    public Defense defendre() {
        return new Defense(1, "se protege avec son épee");
        
    }
    
    @Override
    public Esquive esquive(){
        return new Esquive(4, "recule");
    }
    
    @Override
    public Deplacement deplacer() {
        return new Deplacement(new DistanceUnite(1), "agilement");
    }
   
    
}
