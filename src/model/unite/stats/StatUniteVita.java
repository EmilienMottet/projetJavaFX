/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.stats;

import model.unite.Combattant;
import model.unite.Unite;
import model.unite.stats.TypeStat;
/**
 *
 * @author emimo
 */
public class StatUniteVita extends StatUnite{
    
    public StatUniteVita(Unite proprio, String nom, int valeur) {
        super(proprio, nom, valeur, TypeStat.VITA);
        valeurProperty().addListener((obs,old,newV) -> {proprio.setPointVie(Combattant.PVBASE * this.getValeur() );});
    }
    
}
