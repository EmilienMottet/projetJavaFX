/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.stats;

import model.unite.stats.TypeStat;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.unite.Unite;

/**
 *
 * @author emimo
 */
public class StatUnite extends GestionnaireStats{
    
    private Unite u;
    
    private final StringProperty nom = new SimpleStringProperty("");

    public String getNom() {
        return nom.get();
    }

    public void setNom(String value) {
        nom.set(value);
    }

    public StringProperty nomProperty() {
        return nom;
    }
    private final ObjectProperty<Integer> valeur = new SimpleObjectProperty<>();

    public Integer getValeur() {
        return valeur.get();
    }

    public void setValeur(Integer value) {
        valeur.set(value);
    }

    public ObjectProperty<Integer> valeurProperty() {
        return valeur;
    }

/*    public StatUnite(Unite proprio, String nom){
        this(proprio, nom, 1);
    }*/
    
    public StatUnite(Unite proprio,String nom, int valeur, TypeStat typeStat) {
        super(typeStat);
        u = proprio;
        setNom(nom);
        setValeur(valeur);
        valeurProperty().addListener( (obs,old,newV) -> {if(u.isaDistribTouteSesStats()) {setValeur(old); }});
    }
    
    
}
