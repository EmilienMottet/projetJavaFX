/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.stats;


/**
 *
 * @author emimo
 */
public abstract class GestionnaireStats {
    protected GestionnaireStats next;
    
    protected TypeStat type;
    
    protected GestionnaireStats(TypeStat typeStat){
        this.type = typeStat;
        next = null;
    }
    
    public GestionnaireStats setNext(GestionnaireStats gs){
        next = gs;
        return gs;
    }
    
    public int getValeur(TypeStat typeStat){
        return getValeur(0, typeStat);
    }
    
    private int getValeur(int val,TypeStat typeStat){
        if( typeStat == type ){
            val += getValeur();
        }
        if(next != null){
            val = next.getValeur(val, typeStat);
        }
        return val;
    }
    
 
    protected abstract Integer getValeur();
    
}
