/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.util.ArrayList;
import java.util.List;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.action.Esquive;
import model.unite.stats.StatUnite;
import model.unite.stats.TypeStat;

/**
 *
 * @author emimo
 */
public class CombattantSoigneur extends Combattant{

    CombattantSoigneur(String nom) {
        super(nom);
        getListStats().set(0,new StatUnite(this, "Defense",1,TypeStat.DEFENSE));
        getListStats().set(1,new StatUnite(this, "Puissance",1,TypeStat.ATTAQUE));
        getListStats().set(2,new StatUnite(this, "Vitesse",1,TypeStat.VITESSE));
        getListStats().set(3,new StatUnite(this, "Dissipation",1,TypeStat.ESQUIVE));
        
    }
    public CombattantSoigneur(CombattantSoigneur soigneur){
        super(soigneur);
    }
    
    @Override
    public List<Attaque> attaquer() {
        return new ArrayList<Attaque>() {{ add(new Attaque(5, "heal sa team")) ; }};
        
    }
    
    @Override
    public Esquive esquive(){
        return new Esquive(1, "anticipe le coup");
    }

    @Override
    public Defense defendre() {
        return new Defense(2, "invoque un bouclier magique");        
    }
    
    public Deplacement deplacer() {
        return new Deplacement(new DistanceUnite(1), "prudemment");
    }
    
    
}
