/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.util.Objects;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import model.unite.action.Action;
import model.unite.stats.StatUnite;

/**
 *
 * @author emimo
 */
public abstract class DecorateurRace extends Unite{

    protected Unite unite;
    
    private final StringProperty race = new SimpleStringProperty();
    protected DecorateurRace(Unite unite) {
        setUnite(unite);
    }

    @Override
    public String getNom(){
        return unite.getNom()+" "+race.get();
    }
    
    protected StringProperty raceProperty(){
        return race;
    }
    
    @Override
    public StringProperty nomProperty(){
        return new SimpleStringProperty(Bindings.concat(unite.nomProperty()).concat(" ").concat(race).getValue());
    }

    @Override
    public Action jouer() {
        aptitudeRacial();
        Action action = unite.jouer();
        System.out.println(getNom()+" : j'ai fini de jouer");
        return action;
    }

    @Override
    public BooleanProperty aDistribTouteSesStatsProperty() {
        return unite.aDistribTouteSesStatsProperty(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        return unite.getImage(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<StatUnite> getListStats() {
        return unite.getListStats(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPointDeCompetenceADistrib() {
        return unite.getPointDeCompetenceADistrib(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPointStatTot() {
        return unite.getPointStatTot(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObjectProperty imageProperty() {
        return unite.imageProperty(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IntegerProperty pointDeCompetenceADistribProperty() {
        return unite.pointDeCompetenceADistribProperty(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setImage(Image value) {
        unite.setImage(value); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isaDistribTouteSesStats() {
        return unite.isaDistribTouteSesStats(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListProperty<StatUnite> listStatsProperty() {
        return unite.listStatsProperty(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setListStats(ObservableList value) {
        unite.setListStats(value); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setNom(String value) {
        unite.setNom(value); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPointDeCompetenceADistrib(int value) {
        unite.setPointDeCompetenceADistrib(value); //To change body of generated methods, choose Tools | Templates.
    }

    public void setUnite(Unite unite) {
        this.unite = unite;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DecorateurRace other = (DecorateurRace) obj;
        if (!Objects.equals(this.unite, other.unite)) {
            return false;
        }
        if (!Objects.equals(this.race.get(), other.race.get())) {
            return false;
        }
        return true;
    }


    @Override
    public boolean estEnVie() {
        return unite.estEnVie();
    }

    @Override
    public int getPointVie() {
        return unite.getPointVie();
    }

    @Override
    public void setPointVie(int pv) {
        unite.setPointVie(pv);
    }

    @Override
    public Action getAction() {
        return unite.getAction();
    }

    @Override
    public void setAction(Action action) {
        unite.setAction(action);
    }
    
    public abstract void aptitudeRacial();
}
