/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

import model.unite.stats.TypeStat;

/**
 *
 * @author emimo
 */
public class Deplacement extends Activite{
    
    private String style;


    public Deplacement(DistanceUnite value, String style) {
        super(value.getValue());
        this.style = style;
        setTypeStat(TypeStat.VITESSE);
    }

    public Deplacement(DistanceUnite value) {
        this(value, "normal");
    }    
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
    
}
