/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

import model.unite.stats.TypeStat;

/**
 *
 * @author emimo
 */
public class Esquive extends Activite{
    
    private String style;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Esquive(int value, String style) {
        super(value);
        this.style = style;
        setTypeStat(TypeStat.ESQUIVE);
    }

    public Esquive(int value) {
        this(value, "normal");
    }    
    
}
