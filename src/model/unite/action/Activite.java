/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

import model.unite.stats.TypeStat;

/**
 * a une value et un type
 * @author emimo
 */
public abstract class Activite {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    protected Activite(int value) {
        this.value = value;
    }
    
    private TypeStat typeStat;

    public TypeStat getTypeStat() {
        return typeStat;
    }

    protected void setTypeStat(TypeStat typeStat) {
        this.typeStat = typeStat;
    }
    
    
    
    
    
    
}
