/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

/**
 * contient une fourchette
 * @author emimo
 */
public class Distance {
    private DistanceUnite max;
    
    private DistanceUnite min;

    public Distance(DistanceUnite min, DistanceUnite max) {
        this.max = max;
        this.min = min;
    }
    
    public Distance(){
        max = DistanceUnite.max;
        min = DistanceUnite.min;
    }

    public DistanceUnite getMax() {
        return max;
    }

    public void setMax(DistanceUnite max) {
        this.max = max;
    }

    public DistanceUnite getMin() {
        return min;
    }

    public void setMin(DistanceUnite min) {
        this.min = min;
    }
    
    public boolean estADistance(DistanceUnite u){
        return u.getValue() < max.getValue() && u.getValue() > min.getValue();
        // idealement utiliser un comparator ....
    }
    
}
