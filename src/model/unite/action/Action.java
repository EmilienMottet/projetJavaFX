/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

import java.util.ArrayList;
import java.util.List;
import model.unite.stats.GestionnaireStats;

/**
 *
 * @author emimo
 */
public class Action {
    private Deplacement deplacement;

    
    private Defense defense;
    
    private Esquive esquive;
    private List<Attaque> attaques = new ArrayList<>();
    private GestionnaireStats gestionnaireStats;
    public Action(GestionnaireStats gestionnaireStats) {
        this.gestionnaireStats = gestionnaireStats;
    }
    public Action(){
        
    }
    public Deplacement getDeplacement() {
        return deplacement;
    }
    public void setDeplacement(Deplacement deplacement) {
        this.deplacement = deplacement;
        valueEtStater(this.deplacement);
    }

    public Defense getDefense() {
        return defense;
    }

    public void setDefense(Defense defense) {
        this.defense = defense;
        valueEtStater(this.defense);
    }

    public Esquive getEsquive() {
        return esquive;
    }

    public void setEsquive(Esquive esquive) {
        this.esquive = esquive;
        valueEtStater(this.esquive);
    }
    

    public List<Attaque> getAttaques() {
        return attaques;
    }

    public void setAttaques(List<Attaque> attaques) {
        this.attaques = attaques;
        for (Attaque attaque : attaques) {
            valueEtStater(attaque);
        }
    }    

    /**
     * attribut une valeur a l'activité
     * @param activite 
     */
    private void valueEtStater(Activite activite){
        if(gestionnaireStats != null)
            activite.setValue(activite.getValue()*gestionnaireStats.getValeur(activite.getTypeStat()));
    }

    @Override
    public String toString() {
        return "Action{" + "deplacement=" + deplacement + ", defense=" + defense + ", esquive=" + esquive + ", attaques=" + attaques + ", gestionnaireStats=" + gestionnaireStats + '}';
    }
    
    
}
