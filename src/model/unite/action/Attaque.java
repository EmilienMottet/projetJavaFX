/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

import model.unite.stats.TypeStat;

/**
 * une attaque a plein de cas possible aoe, soin, ...
 * @author emimo
 */
public class Attaque extends Activite{

    private boolean multicible;
    
    private boolean soin;
    
    private Distance distance;

    
    
    private String style;


    public Attaque(int value, String style) {
        this(value, false, style);
    }

    public Attaque(int value) {
        this(value,false,"normal");
    }

    public Attaque(int value, boolean multicible, String style) {
        this(value, multicible, false, style);
    }

    public Attaque(int value, boolean multicible, boolean soin, String style) {
        this(value, multicible, soin, new Distance(new DistanceUnite(0), DistanceUnite.max), style);
    }

    public Attaque(int value, boolean multicible, boolean soin, Distance distance, String style) {
        super(value);
        this.multicible = multicible;
        this.soin = soin;
        this.distance = distance;
        this.style = style;
        setTypeStat(TypeStat.ATTAQUE);
    }
    public boolean isMulticible() {
        return multicible;
    }
    public void setMulticible(boolean multicible) {
        this.multicible = multicible;
    }
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
    
    
    public boolean isSoin() {
        return soin;
    }

    public void setSoin(boolean soin) {
        this.soin = soin;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }
    
    
    
    
    
}
