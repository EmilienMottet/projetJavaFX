/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite.action;

/**
 * pour que une distance ne soit pas dependante d'un type ( imaginons qu'on peut avoir des float et des int ou autres ! )
 * @author emimo
 */
public class DistanceUnite {
    private int value;

    public int getValue() {
        return value;
    }

    private void setValue(int value) {
        this.value = value;
    }

    static final public DistanceUnite max =  new DistanceUnite(10);

    static final public DistanceUnite min =  new DistanceUnite(10);
    
    
    public DistanceUnite(int value) {
        this.value = value;
    }
    
    
}
