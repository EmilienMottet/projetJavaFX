/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author emimo
 */
public class FabriqueCombattant {
    /**
     * retourne un combattant en fonction du nom
     * @param typeCombattant
     * @param nom
     * @return 
     */
    public static Unite getCombattant(TypeCombattant typeCombattant ,String nom) {
        Combattant combattant = null;
        switch(typeCombattant){
            case ARCHER : combattant = new CombattantArcher(nom); break;
            case ASSASSIN : combattant = new CombattantAssassin(nom); break;
            case MAGE : combattant = new CombattantMage(nom); break;
            case SOIGNEUR : combattant = new CombattantSoigneur(nom); break;
            case TANK : combattant = new CombattantTank(nom); break;
            default: combattant = null;
        }
        
        return combattant;
    }
    /**
     * constructeur par copy
     * @param combattant
     * @return 
     */
    public static Unite getCombattant(Unite combattant){
        try {
             return combattant.getClass().getConstructor(combattant.getClass()).newInstance(combattant);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(FabriqueCombattant.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
