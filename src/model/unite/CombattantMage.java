    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;


import java.util.ArrayList;
import java.util.List;
import model.unite.action.Attaque;
import model.unite.action.Defense;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.action.Esquive;


/**
 *
 * @author emimo
 */
public class CombattantMage extends Combattant{

    CombattantMage(String nom) {
        super(nom);
        
    }
    public CombattantMage(CombattantMage mage){
        super(mage);
    }
   
    // peut etre mettre un bool pour changer les attaques ( genre un coup sur 2 diffénrent ) ou en fonction des distance 
    @Override
    public List<Attaque> attaquer() {
        return new ArrayList<Attaque>() {{ add(new Attaque(8, "tir à l'attaque")); }} ;        
    }

    @Override
    public Defense defendre() {
        return new Defense(1, "invoque un bouclier magique");
    }
    
    @Override
    public Esquive esquive(){
        return new Esquive(3, "bloque grace à sa magie");
    }
    
    @Override
    public Deplacement deplacer() {
        return new Deplacement(new DistanceUnite(1), "vole");
    }
    
    
}
