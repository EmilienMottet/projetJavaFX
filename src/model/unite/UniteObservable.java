/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emimo
 */
public abstract class UniteObservable {
    private List<Observateur> listObservateurs = new ArrayList<>();
    /**
     * notify un changement
     */
    public void notifier() {
        for (Observateur listObservateur : listObservateurs) {
            listObservateur.update(this);
        }
    }
    
    /**
     * ajoute des observateur
     * @param observateur 
     */
    public void ajouterObservateur(Observateur observateur) {
        listObservateurs.add(observateur);
    }

    
    public void supprimerObservateur(Observateur observateur) {
        listObservateurs.remove(observateur);
    }

}
