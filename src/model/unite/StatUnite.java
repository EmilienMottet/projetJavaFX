/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.unite;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author emimo
 */
public class StatUnite {

    private final StringProperty nom = new SimpleStringProperty("");
    private final IntegerProperty valeur = new SimpleIntegerProperty(0);
    public StatUnite(String nom, int valeur) {
        setNom(nom);
        setValeur(valeur);
    }

    public String getNom() {
        return nom.get();
    }

    public void setNom(String value) {
        nom.set(value);
    }

    public StringProperty nomProperty() {
        return nom;
    }

    public int getValeur() {
        return valeur.get();
    }

    public void setValeur(int value) {
        valeur.set(value);
    }

    public IntegerProperty valeurProperty() {
        return valeur;
    } 
    
}
