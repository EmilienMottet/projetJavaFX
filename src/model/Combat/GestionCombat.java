/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Combat;

import model.unite.Unite;
import model.unite.action.Action;

/**
 *
 * @author emimo
 */
@FunctionalInterface
public interface GestionCombat {
    Action combat(Unite unite1, Unite unite2);
}
