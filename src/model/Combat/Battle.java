/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Combat;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.joueur.Team;
import model.unite.action.Deplacement;
import model.unite.action.DistanceUnite;
import model.unite.Observateur;
import model.unite.Unite;
import model.unite.UniteObservable;
import model.unite.action.Action;
import model.unite.action.Attaque;

/**
 *
 * @author emimo
 */
public class Battle extends Observable implements Observateur{

    private final StringProperty message = new SimpleStringProperty("en attente du vainqueur ...");

    public String getMessage() {
        return message.get();
    }

    public void setMessage(String value) {
        Platform.runLater(() -> message.set(value));
    }

    public StringProperty messageProperty() {
        return message;
    }
    
    private Unite[][] battleground ;
    
    private Map<Unite,Integer[]> postion = new HashMap<>();
    
    private Team team1;
    
    private Team team2;
    
    private Team winner = null;

    public Team getWinner() {
        return winner;
    }
    
    

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    
    /**
     * 
     * @param team1
     * @param team2
     * @param uniteTaille
     * @param gestionCombat 
     */
    public Battle(Team team1, Team team2, DistanceUnite uniteTaille,GestionCombat gestionCombat) {
        this.gestionCombat = gestionCombat;
        int taille = uniteTaille.getValue();
        team1.typer();
        team2.typer();
        setTeam1(team1);
        setTeam2(team2);
        battleground = new Unite[Team.getTaille()][taille];
        positionnerUnite();
        System.out.println("team 1 : "+team1);
        System.out.println("team 2 : "+team2);
        System.out.println("map : "+battleground[0][taille-1]);
        System.out.println("map : "+battleground[0][0]);
        System.out.println(postion);

    }
    
    /**
     * lance le positionnement de team
     */
    private void positionnerUnite(){
        positionnerTeam(team1, 0+1);
        positionnerTeam(team2,battleground[0].length-1 - 1);
    }
    
    /**
     * positionne la team soit en 1 soit en lenght -2
     * @param team
     * @param cote 
     */
    private void positionnerTeam(Team team, int cote){
        for (int i = 0 ; i< battleground.length ; ++i){
            
            battleground[i][cote] = team.getListUnit().get(i);
            Integer[] tab = new Integer[2];
            tab[0]=i;
            tab[1]=cote;
            postion.put(team.getListUnit().get(i), tab);
        }
    }
    /**
     * demare la battle
     */
    public void startBattle(){
        demarerTeam(team1);
        demarerTeam(team2);
        setMessage("on est dans le constructeur");
        notif();
    }
    /**
     * notify un changement d'etat
     */
    private void notif(){
        setChanged();
        notifyObservers();
    }
    /**
     * observe l'unite et lance son thread
     * @param team 
     */
    private void demarerTeam( Team team){
        for (Unite u : team.getListUnit()) {
            u.ajouterObservateur(this);
            u.lanceThread();
        }
    }
    /**
     * retourne la possition de l'unite sur la ligne 
     * @param unite
     * @return 
     */
    public int getPositionSurLaLigne(Unite unite){
         Integer[] tab = postion.get(unite);
         if(tab != null) return tab[1];
         return -1;
    }
    /**
     * retourne la possition de l'unite sur la colonne
     * @param unite
     * @return 
     */
    public int getPositionSurLaColonne(Unite unite){
         Integer[] tab = postion.get(unite);
         if(tab != null) return tab[0];
         return -1;
    }
    /**
     * place une unite sur une ligne
     * @param unite
     * @param deplacement 
     */
    public void setPositionSurLaLigne(Unite unite,Deplacement deplacement ){
        int value = deplacement.getValue(); // idéalement faudrait remplacer les int par des DistanceUnite ...
        Integer[] tab = postion.get(unite);
        int maPos = tab[1];
        int posAdv = getPositionSurLaLigne(trouverAdversaireFaceAFace(unite));
        if( maPos < posAdv){
            if(maPos+value < posAdv){
                maPos += value;
            }else{
                maPos = posAdv - 1;
            }
        }
        else{
            if(maPos - value > posAdv){
                maPos -= value;
            }
            else{
                maPos = posAdv + 1 ;
            }
        }
        // quand enemmie meurt on le met sur la case 0 est la case lenght -1
        
        tab[1] = maPos;
        
        testVictoire(maPos, unite);
        
    }
    /**
     * test si il une unite a gagner ( cad elle est en case 1 ou lenght -2 )
     * @param maPos
     * @param unite 
     */
    private void testVictoire(int maPos, Unite unite){
        if(maPos == 1 || maPos == battleground[0].length - 2 ){
            if(team1.getListUnit().contains(unite))
                winner = team1;
            else
                winner = team2;
            
            System.out.println("il y a une vainqueur");
            setMessage("le vainqueur est "+winner.getProprietaire().getNom());
            arreterPartie();
            notif();
        }
    }
    /**
     * termine la partie en tuant les joueurs
     */
    private void arreterPartie(){
        tuerTeam(team1);
        tuerTeam(team2);
    }
    /**
     * tue les les joueurs de la team
     * @param team 
     */
    private void tuerTeam(Team team){
        for (Unite unite : team.getListUnit()) {
            unite.setPointVie(0);
        }
    }
    /**
     * met au prugatoire l'unite ( cad la case 0 ou lenght -1 )
     * @param unite 
     */
    private void mettreAuPurgatoire(Unite unite){
        Integer[] tab = postion.get(unite);
        int maPos = tab[1];
        int posAdv = getPositionSurLaLigne(trouverAdversaireFaceAFace(unite));
        if( maPos > posAdv){
            tab[1] = battleground[0].length - 1;
        }
        else{
           tab[1] = 0;
        }
    }
    /**
     * retourne l'adversaire en face de l'unite
     * @param unite
     * @return 
     */
    public Unite trouverAdversaireFaceAFace(Unite unite){
        
        
        for (Map.Entry<Unite,Integer[]> entry : postion.entrySet() ) {
            if( getPositionSurLaColonne(entry.getKey()) == getPositionSurLaColonne(unite) && ! entry.getKey().equals(unite)) return entry.getKey();
        }
        
        return null;
    }

    
    private GestionCombat gestionCombat;
    
    /**
     * recoit une info de l'unite qui a effectuer une action
     * @param uniteObservable 
     */
    @Override
    public void update(UniteObservable uniteObservable) {
        if(uniteObservable instanceof Unite){
            Unite unite = (Unite) uniteObservable;
            if(!unite.estEnVie()){
                mettreAuPurgatoire(unite);
                System.out.println("on a mit au purg"+" ma pos ("+getPositionSurLaColonne(unite)+","+getPositionSurLaLigne(unite)+") mes pv : "+unite.getPointVie());
            }
            else{    
                setPositionSurLaLigne(unite, unite.getAction().getDeplacement());
                
                System.out.println(unite+" ma pos ("+getPositionSurLaColonne(unite)+","+getPositionSurLaLigne(unite)+") mes pv : "+unite.getPointVie());
                Action actionrealise = gestionCombat.combat(unite, trouverAdversaireFaceAFace(unite));
                if(!actionrealise.getAttaques().isEmpty()) setMessage(faireMessage(unite,actionrealise));
            }

        }

    }
    /**
     * calcul la distance entre 2 unites
     * @param u1
     * @param u2
     * @return 
     */
    public DistanceUnite calculDistance(Unite u1, Unite u2){
        return new DistanceUnite(Math.abs(getPositionSurLaLigne(u1)-getPositionSurLaLigne(u2)));
    }
    
    // message peut etre le mettre dans une autre classe car la ca peut encombrer

    //c pas bien d'y faire ici car on respecter plus le single responsability
    /**
     * retourne un string qui décrit l'action
     * @param unite
     * @param realiser
     * @return 
     */
    private String faireMessage(Unite unite,Action realiser) {
        String mess ="";
        mess +=unite.getNom()+ " attaque : ";
        for (Attaque a  : realiser.getAttaques()) {
            mess+= a.getStyle()+" " ;
        }
        mess+=" son adv ("+trouverAdversaireFaceAFace(unite).getNom()+") ";
        
        if(realiser.getEsquive() != null){
            mess += realiser.getEsquive().getStyle()+" ";
        }else{
            mess += realiser.getDefense().getStyle()+" ";
        }
        
        return mess;
    }
    
    
    
}
